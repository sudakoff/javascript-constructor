import image from './assets/image.jpg'
import {TextBlock, TitleBlock, ImageBlock, TextColumns} from './classes/blocks'
import {css} from './utils'


export const model = [
    new TitleBlock ('Конструктор на ЖС', {
        styles: css({
            background: 'linear-gradient(to right, #ff0099, #493240)',
            color: '#fff',
            padding: '1.5rem',
            'text-align': 'center'
          }),
        tag: 'h2'
    }),
    new TextBlock ('Какой-либо интересный текст', {
        styles: css({
            background: 'darkred',
            padding: '1.5rem',
            color: '#fff',
            'text-align': 'center'
          }),
    }),
    new TextColumns ([
        'Пример текста 1',
        'Пример текста 2',
        'Пример текста 3',
        'Пример текста 4'
    ], {
        styles: css({
            background: 'linear-gradient(to right, #ff0099, #493240)',
            color: '#fff',
            padding: '1.5rem',
            'text-align': 'center'
          }),
    }),
    new ImageBlock (image, {
        styles: css({
            padding: '2rem 0',
            display: 'flex',
            'justify-content': 'center'
          }),
        alt: 'Котики',
        imageStyles: 'width:100px'
    })
]
